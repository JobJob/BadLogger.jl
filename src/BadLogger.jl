module BadLogger
export create_logger, setup_logging, nowpath, restore_og_logger, loghparams

include("formatto_loggo.jl") # same same SimpleLogger but different
include("logging_utils.jl")      # logging setup
include("tenserbored_utils.jl")  # loghparams

end
