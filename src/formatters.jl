function barefmt(level, _module, group, id, file, line)
    return (Logging.default_logcolor(level), "$level", "")
end

"""
So I wanted line numbers even for `@info` logs, so I took [`default_metafmt(...)`](https://github.com/JuliaLang/julia/blob/6aaedecc447e3d8226d5027fb13d0c3cbfbfea2a/stdlib/Logging/src/ConsoleLogger.jl#L60)
from Base and commented line 5 to get 'em. But I think for non-`ConsoleLogger`s
you get them anyway, so this is unused atm.
"""
function showline_fmt(level::LogLevel, _module, group, id, file, line)
    @nospecialize
    color = Logging.default_logcolor(level)
    prefix = (level == Logging.Warn ? "Warning" : string(level))*':'
    suffix = ""
    # Info <= level < Warn && return color, prefix, suffix
    _module !== nothing && (suffix *= "$(_module)")
    if file !== nothing
        _module !== nothing && (suffix *= " ")
        suffix *= Base.contractuser(file)
        if line !== nothing
            suffix *= ":$(isa(line, UnitRange) ? "$(first(line))-$(last(line))" : line)"
        end
    end
    !isempty(suffix) && (suffix = "@ " * suffix)
    return color, prefix, suffix
end
