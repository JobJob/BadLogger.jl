using Logging: SimpleLogger, LogLevel, Warn, Info
import Logging: handle_message
using Parameters: @unpack

"""
Copied from https://github.com/JuliaLang/julia/blob/bc4d49a67a406cb1cbb4c399d3663ea4d0998d45/base/logging.jl#L661
with some minor modifications to
a) unpack log_args
b) not print the location info if
`group == :no_loc` or module===nothing or level is Info, i.e. `@info "me log" _group=:no_loc`
"""
function writea_di_loggo(ioc::IOContext, log_args)
    @unpack level, message, _module, group, id, file, line, kwargs = log_args
    maxlog = get(kwargs, :maxlog, nothing)
    if maxlog isa Core.BuiltinInts
        remaining = get!(logger.message_limits, id, Int(maxlog)::Int)
        logger.message_limits[id] = remaining - 1
        remaining > 0 || return
    end
    show_loc = group != :no_loc && _module !== nothing && level != Info
    levelstr = level == Warn ? "Warning" : string(level)
    msglines = split(chomp(string(message)::String), '\n')
    show_loc && print(ioc, "┌ ")
    println(ioc, levelstr, ": ", msglines[1])
    for i in 2:length(msglines)
        println(ioc, "│ ", msglines[i])
    end
    for (key, val) in kwargs
        key === :maxlog && continue
        println(ioc, "│   ", key, " = ", val)
    end
    if show_loc
        println(ioc, "└ @ ", _module, " ", file, ":", line)
    end
    return nothing
end
