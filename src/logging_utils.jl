#=
Calling setup_logging() will set logging up to be verbose so we can see the full
contents of arrays, including printing out vectors on one line (higher dim arrays
will be formatted as default)

To stop your log messages displaying:
1) use @debug (disableable with `JULIA_DEBUG="ModuleName1,ModuleName1" julia ...`)
2) Filter using the log "groups" that are hidden by default - :stupid, :ludicrous,
    e.g. `@info "blah" stuff _group=:stupid`
    to enable one of these groups, edit `filtered_groups` below
3)
    a) add a group to your log message: `@info "blah" stuff _group=:nevernude`
    b) start julia with env var JULIA_HIDE_GROUPS set, e.g.
       `JULIA_HIDE_GROUPS="nevernude, magician" julia ...`
=#

using Logging: Logging, global_logger
using Dates: Dates, @dateformat_str, now

using DataStructures: DefaultDict

using LoggingExtras: BelowMinLevel, Info, EarlyFilteredLogger, TransformerLogger,
    MinLevelLogger, FormatLogger

const ye_olde_logger = Ref(global_logger())

function restore_og_logger()
    global_logger(ye_olde_logger[])
end

function async_close(fh)
    return @async close($fh)
end

"""
Create a logger to the IO stream provided with:
1) some filters including log groups set via `filtered_groups[]`
2) debug set using comma separated module names in ENV["JULIA_DEBUG"]
3) it'll always flush after every log (if `always_flush` is true)
4) adds timestamps unless kwarg timestamps is false
5) removes code location if linenums is false
"""
function create_logger(destfile::String; kws...)
    logfh = open(destfile, "w")
    return create_logger(logfh; kws...)
end

function create_logger(dest::IO=stderr;
 always_flush=true, timestamps=true, linenums=true, minlevel=Info)
    update_logfilters!()
    basic_logger = FormatLogger(writea_di_loggo, dest)
    logger = EarlyFilteredLogger(loggo_filtero, basic_logger)
    timestamps && (logger = timestamp_logger(logger))
    linenums || (logger = no_linenums_logger(logger))
    # Without minlevel=Info, GPUCompiler debug logging pollutes everything, need a better
    # workaround, in the meantime no @debug for now (ref: https://github.com/JuliaGPU/GPUCompiler.jl/issues/271)
    logger = MinLevelLogger(logger, minlevel)
    return logger
end

"""
Create a logger and set it as the global logger
"""
function setup_logging(destfile::String; kws...)
    logfh = open(destfile, "w")
    return setup_logging(logfh; kws...)
end

function setup_logging(dest::IO=stderr; kws...)
    logger = create_logger(dest; kws...)
    @info "BadLogger.create_logger: switching to new logger... now ($(datetimestr()))" logger
    prev_logger = global_logger(logger)
    @info "first!!!!!!!!" # I always win!
    return prev_logger
end

const date_format = dateformat"yyyy-mm-dd HH:MM:SS.sss"
datetimestr() = Dates.format(now(), date_format)

function add_timestamp(log)
    return merge(log, (; message = "$(datetimestr()) | $(log.message)"))
end

function filter_codeloc(log)
    return merge(log, (; _module=nothing)) # see formatto_loggo.jl for details
end

function timestamp_logger(logger)
    return TransformerLogger(add_timestamp, logger)
end

function no_linenums_logger(logger)
    return TransformerLogger(filter_codeloc, logger)
end

function comma_sep_to_arr(arrstr)
    return String.(split(arrstr, r",\s*"; keepempty=false))
end

"""
This needs to be a DefaultDict and not just a set, to handle the
ENV["JULIA_DEBUG"] = "all" case
"""
function get_debug_moddic()
    DDType = DefaultDict{String,Bool,Bool}
    dbgmodstr = get(ENV, "JULIA_DEBUG", "")
    dic =
        if dbgmodstr == "all"
            DDType(true)
        else
            dbgmodules = comma_sep_to_arr(dbgmodstr)
            DDType(false, Dict(dbgmodules .=> true))
        end
    return dic
end

const debug_modules = Ref(get_debug_moddic())
const filtered_groups = Ref(Set{Symbol}((:stupid, :ludicrous)))

function set_filtered_log_groups!()
    hidden_groups_str = get(ENV, "JULIA_HIDE_GROUPS", "")
    if !isempty(strip(hidden_groups_str))
        bad_groups_strs = comma_sep_to_arr(hidden_groups_str)
        # handle case where accidentally do `JULIA_HIDE_GROUPS=":dingbat, :frankencow"`
        # which then become ::dingbat unt ::frankencow
        good_groups_strs = lstrip.(bad_groups_strs, ':')
        filtered_groups[] = union(filtered_groups[], Set(Symbol.(good_groups_strs)))
    end
    return filtered_groups[]
end

"""
Update globals used in log filtering
"""
function update_logfilters!()
    set_filtered_log_groups!()
    debug_modules[] = get_debug_moddic()
    return filtered_groups[], debug_modules[]
end

"""
Filter out messages in the filtered groups.
Filter out debug messages from modules not specified in `ENV["JULIA_DEBUG"]`
"""
function loggo_filtero(log)
    #log fields: level, message, _module, group, id, file, line, ...
    badgroup = log.group in filtered_groups[]
    badmodule = log.level <= Logging.Debug &&
        !(debug_modules[][string(log._module)] ||
            debug_modules[][string(parentmodule(log._module))])
    return !badgroup && !badmodule
end

nowpath() = replace("$(now())", ":"=>"-")

function check_debug()
    # will run from module BadLogger so can be used to check
    # Env["JULIA_DEBUG"]="BadLogger" is working
    @debug "so yo BadLog"
end

function __init__()
    ye_olde_logger[] = global_logger()
end
