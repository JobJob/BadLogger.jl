# not including this file in the package for now...
# messes with pre-compilation (well causes warnings at
# least), and we're using different loggers now so
# may not even need these)

# Print out vectors on one line in full precision
const FullFiguredTypes = Union{Float64, Float32}
function Base.show(io::IO, x::AbstractVector{<:FullFiguredTypes})
    print(io, "[ ")
    foreach(i->print(io, string(x[i]), ", "), 1:size(x, 1)-1)
    length(x) > 0 && print(io, string(x[end]), " ")
    print(io, "]")
end

function Base.show(io::IO, x::AbstractMatrix{<:FullFiguredTypes})
    print(io, "[\n")
    for i in 1:size(x, 1)
        foreach(j->print(io, string(x[i, j]), ", "), 1:size(x, 2)-1)
        print(io, string(x[i, end]))
        i != size(x, 1) && print(io, ";\n\n")
    end
    print(io, " ]")
end

function namespropsstr(x)
    props = propertynames(x)
    vals = getproperty.(Ref(x), props)
    return join(("$p: $v" for (p,v) in zip(props, vals)), ", ")
end

function Base.show(io::IO, x)
    props = propertynames(x)
    vals = getproperty.(Ref(x), props)
    print(io, namespropsstr(x), "\n")
end
