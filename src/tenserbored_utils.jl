using TensorBoardLogger: log_text, TBLogger

"""
`loghparams(logger::TBLogger, name, dict_or_namedtuple)`

Use it to log hyperparams as text to tensorboard, e.g.
```
x = (a=1, b="2")
loghparams(tblogger, "config_brug", x)
```

`dict_or_namedtuple` can be anything for which `pairs(dict_or_namedtuple)` can
be called on right.
"""
function loghparams(tblogger::TBLogger, name, x)
    for (k,v) in pairs(x)
        log_text(tblogger,  name*"/$k", "$v")
    end
end
