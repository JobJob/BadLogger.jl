using TensorBoardLogger: TBLogger, tb_overwrite
using BadLogger: loghparams
using Test

@testset "tenso boardo" begin
    tblogdir = "badlogger_tbtesto"
    tblogger = TBLogger(tblogdir, tb_overwrite)
    ccc = (yes="bum", base_seed=0x12312af, wow=10, hmmm="1")
    loghparams(tblogger, "fah yeah", ccc)
    @test 1==1
    run(`rm -r $tblogdir`)
end
